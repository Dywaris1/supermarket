<%@ page import="java.util.Map" %>
<%@ page import="Article.Article" %><%--
  Created by IntelliJ IDEA.
  User: julie
  Date: 26/02/2020
  Time: 09:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>supermarché</title>
      <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
  </head>
  <body>

  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">

      <button class="btn btn-outline-info mr-auto " style=" margin-bottom: 14px; margin-right: 15px;" onclick="addArticle()">
       Ajouter un article
      </button>

          <form class="form-inline row justify-content-end" action="GestionArticle" method="get">
              <div class="row justify-content-end">
                  <input id="nom" class="form-control col-4" placeholder="Login" aria-label="Login" type="text" name="nom"/>
                  <input id="pwd" class="form-control  col-4" placeholder="Password" aria-label="Password" type="password" name="pwd"/>
                  <button class="btn btn-outline-info my-2 my-sm-0 col-4" type="submit"><span
                          style="vertical-align: inherit;"><span style="vertical-align: inherit;">Connexion</span></span></button>
              </div>
          </form>



  </nav>
  <%
    Map<Long, Article> articles = (Map<Long, Article>) request.getAttribute("articles");
    out.println("<table class='table'>");
    out.println("<thead><tr>" +
            "<td>code barre</td>" +
            "<td>reference</td>" +
            "<td>libelle</td>" +
            "<td>prix HT</td>" +
            "<td>taux TVA</td>" +
            "</tr></thead><tbody>");
    for(Long key: articles.keySet()) {
        out.println("<tr>");
        out.println("<td>"+articles.get(key).codeBarre + "</td>");
        out.println("<td>"+articles.get(key).reference + "</td>");
        out.println("<td>"+articles.get(key).libelle + "</td>");
        out.println("<td>"+(float)articles.get(key).prixHT/100 + "</td>");
        out.println("<td>"+(float)articles.get(key).tauxTVA/100 + "</td>");
        out.println("</tr>");
    }
      out.println("</tbody></table>");
  %>
  <h1 style="text-align: center; margin-top: 10px;"><span class="badge badge-info" >Liste de course</span></h1>

  <form action="" method="post">
      <div id="article"></div>
      <button class="btn btn-outline-info" style="float: right;margin-right: 10px;"  type="submit">
          Generer un ticket
      </button>


  </form>


  </body>
<script>
  var article = document.getElementById("article");
  function addArticle(){
    field =

        '<input style="margin-right: auto;\n' +
        'margin-left: auto;\n' +
        'width: 60%;" type="text" class="form-control" id="code" name="code" placeholder="Code"/> ' +

        '<input style="margin-right: auto;\n' +
        'margin-left: auto;\n' +
        'width: 60%;" class="form-control" type="number" id="nb" name="nb" placeholder="quantite"/> ' +
        '<br/> <div class="page-header">';
    article.insertAdjacentHTML("beforeend",field);
  }
</script>
</html>
