import Article.Article;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("/GestionArticle")
public class GestionArticle extends HttpServlet {

    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        Long codeBarre = Long.parseLong(req.getParameter("codebarre"));
        System.out.println(codeBarre);
        String reference = req.getParameter("reference");
        System.out.println(reference);
        String libelle = req.getParameter("libelle");
        System.out.println(libelle);
        int prixHT = Integer.parseInt(req.getParameter("prixHT"));
        System.out.println(prixHT);
        int tauxTVA = Integer.parseInt(req.getParameter("tauxTVA"));
        System.out.println(tauxTVA);




        Article newArticle = new Article(codeBarre,reference,libelle,prixHT,tauxTVA);
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long,Article> articles = singletonList.getListArticle();
        articles.put(newArticle.codeBarre,newArticle);
        this.afficheArticle(response);
    }
    private void afficheArticle(HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        ServletOutputStream out = response.getOutputStream();
        out.println("<html><head>");
        out.println("<title>Gestion des articles</title>");
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.css\">");
        out.println("</head><body>");
        out.println("<h1 style=\"text-align: center; margin-top: 10px;\"><span class=\"badge badge-info\" >Liste des articles</span></h1>");
        out.println("<table class='table'>");
        out.println("<tr>" +
                "<td>code barre</td>" +
                "<td>reference</td>" +
                "<td>libelle</td>" +
                "<td>prix HT</td>" +
                "<td>taux TVA</td>" +
                "<td></td>" +
                "<td></td>" +
                "</tr>");
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long,Article> articles = singletonList.getListArticle();
        for(Long key: articles.keySet()) {
            out.println("<tr>");
            out.println("<td>"+articles.get(key).codeBarre + "</td>");
            out.println("<td>"+articles.get(key).reference + "</td>");
            out.println("<td>"+articles.get(key).libelle + "</td>");
            out.println("<td>"+articles.get(key).prixHT + "</td>");
            out.println("<td>"+articles.get(key).tauxTVA + "</td>");
            out.println("<td><a href='./GestionArticleSuppression?code="+articles.get(key).codeBarre+"'>Supprimer</a>");
            out.println("<td><a href='./GestionArticleModification?code="+articles.get(key).codeBarre+"'>Modifier</a>");
            out.println("</tr>");
        }
        out.println("</table>");
        out.println("<h1 style=\"text-align: center; margin-top: 10px;\"><span class=\"badge badge-info\" >Ajouter un article</span></h1>");

        out.println("<form action=\"GestionArticle\" method=\"post\">" +
                ""+
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"codebarre\" id=\"codebarre\" placeholder=\"Code barre\"></br>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"reference\" id=\"reference\" placeholder=\"Reference\"></br>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"libelle\" id=\"libelle\" placeholder=\"libelle\"></br>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"prixHT\" id=\"prixHT\" placeholder=\"Prix HT\"></br>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"tauxTVA\" id=\"tauxTVA\" placeholder=\"Taux TVA\"></br>" +
                "<button style=\"margin-right: auto; margin-left:auto;\" class=\"btn btn-outline-info form-control\" type=\"submit\">Valider</button>  </form>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.afficheArticle(response);
    }
}
