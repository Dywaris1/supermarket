import Article.Article;
import java.util.HashMap;
import java.util.Map;

public class MapArticle {
     private Map<Long, Article> listArticle;
     private boolean isAuth = false;

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean auth) {
        isAuth = auth;
    }

    public Map<Long, Article> getListArticle() {
        return listArticle;
    }

    public void setListArticle(Map<Long, Article> listArticle) {
        this.listArticle = listArticle;
    }

    private static MapArticle INSTANCE = new MapArticle();

    private MapArticle(){
        listArticle = new HashMap<>();
        listArticle.put((long) 1,new Article(1,"fruit","pomme",100,2));
        listArticle.put((long) 2,new Article(2,"fruit","poire",150,2));
        listArticle.put((long) 3,new Article(3,"fruit","orange",200,2));
        listArticle.put((long) 4,new Article(4,"Viande","boeuf",4,1));
        listArticle.put((long) 5,new Article(4,"Viande","poulet",7,1));
        listArticle.put((long) 5,new Article(4,"Poisson","saumon",15,1));
        listArticle.put((long) 5,new Article(4,"Poisson","truite",15,1));




    }
    public static final MapArticle getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MapArticle();
        }
        return INSTANCE;
    }
}
