import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import Article.Article;
import java.util.Map;
import Article.*;
@WebServlet("/GestionArticleModification")
public class GestionArticleModification extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
        Long codeBarre = Long.parseLong(req.getParameter("codebarre"));
        String reference = req.getParameter("reference");
        String libelle = req.getParameter("libelle");
        int prixHT = Integer.parseInt(req.getParameter("prixHT"));
        int tauxTVA = Integer.parseInt(req.getParameter("tauxTVA"));

        MapArticle singletonList = MapArticle.getInstance();
        Map<Long,Article> articles = singletonList.getListArticle();

        articles.replace(codeBarre, new Article(codeBarre,reference,libelle,prixHT,tauxTVA));


        ServletContext context= getServletContext();
        RequestDispatcher rd= context.getRequestDispatcher("/GestionArticle");
        rd.forward(req, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long,Article> articles = singletonList.getListArticle();

        Article modifArticle = articles.get(Long.parseLong(request.getParameter("code")));

        response.setContentType("text/html");
        ServletOutputStream out = response.getOutputStream();
        out.println("<html><head>");
        out.println("<title>Modification</title>");
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.css\">");
        out.println("</head><body>");
        out.println("<h1 style=\"text-align: center; margin-top: 10px;\"><span class=\"badge badge-info\" >Modification d'un article</span></h1>");
        out.println("<form action=\"GestionArticleModification\" method=\"post\">" +
                "<input type=\"hidden\" name=\"codebarre\" id=\"codebarre\" value="+modifArticle.codeBarre+"></br>" +
                "<label style=\"margin-left: 256px;\" for=\"reference\"><h4>Reference</h4></label>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"reference\" id=\"reference\" value="+modifArticle.reference+"></br>" +
                "<label style=\"margin-left: 256px;\"  for=\"libelle\"><h4>Libelle</h4></label>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"libelle\" id=\"libelle\" value="+modifArticle.libelle+"></br>" +
                "<label style=\"margin-left: 256px;\" for=\"prixHT\"><h4>Prix HT</h4></label>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"prixHT\" id=\"prixHT\" value="+modifArticle.prixHT+"></br>" +
                "<label style=\"margin-left: 256px;\" for=\"tauxTVA\"><h4>Taux TVA</h4></label>" +
                "<input class=\"form-control\" style=\"margin-right: auto; margin-left: auto; width: 60%\" type=\"text\" name=\"tauxTVA\" id=\"tauxTVA\" value="+modifArticle.tauxTVA+"></br>" +
                "<button style=\"margin-right: auto; margin-left:auto;\" class=\"btn btn-outline-info form-control\" type=\"submit\">Valider</button>  </form>");
    }
}
