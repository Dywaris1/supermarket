package Article;

import javax.servlet.ServletOutputStream;
import java.io.IOException;

public class ServiceStyle {

    public static ServletOutputStream navbarComponent(ServletOutputStream out) throws IOException {
        out.println("<nav class=\"navbar navbar-dark bg-dark\">\n" +
                " <a class=\"navbar-brand\" href=\"#\">Navbar</a>\n" +
                "  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\" aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n" +
                "    <span class=\"navbar-toggler-icon\"></span>\n" +
                "  </button>\n" +
                "  <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n" +
                "    <ul class=\"navbar-nav\">\n" +
                "      <li class=\"nav-item active\">\n" +
                "        <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n" +
                "      </li>\n" +
                "      <li class=\"nav-item\">\n" +
                "        <a class=\"nav-link\" href=\"#\">Features</a>\n" +
                "      </li>\n" +
                "      <li class=\"nav-item\">\n" +
                "        <a class=\"nav-link\" href=\"#\">Pricing</a>\n" +
                "      </li>\n" +
                "      <li class=\"nav-item\">\n" +
                "        <a class=\"nav-link disabled\" href=\"#\" tabindex=\"-1\" aria-disabled=\"true\">Disabled</a>\n" +
                "      </li>\n" +
                "    </ul>\n" +
                "  </div>" +
                "</nav>");
        return out;
    }
}
