import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import Article.Article;
import java.util.Map;

@WebServlet("/GestionArticleSuppression")
public class GestionArticleSuppression extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long,Article> articles = singletonList.getListArticle();
        articles.remove(Long.parseLong(request.getParameter("code")));
        ServletContext context= getServletContext();
        RequestDispatcher rd= context.getRequestDispatcher("/GestionArticle");
        rd.forward(request, response);
    }
}
