import Article.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@WebServlet("")
public class Ticket extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long, Article> articles = singletonList.getListArticle();
        String[] codes = request.getParameterValues("code");
        String[] quantites = request.getParameterValues("nb");
        float coutTotal =0;
        response.setContentType("text/html");
        ServletOutputStream out = response.getOutputStream();
        out.println("<html><head>");
        out.println("<title>Enregistrement</title>");
        out.println("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.css\">");
        out.println("</head><body>");
        out = ServiceStyle.navbarComponent(out);
        out.println("<table class='table'>");
        out.println("<thead><tr>" +
                "<td>code barre</td>" +
                "<td>reference</td>" +
                "<td>libelle</td>" +
                "<td>prix HT</td>" +
                "<td>quantite</td>" +
                "<td>taux TVA</td>" +
                "<td>cout </td>" +
                "</tr></thead>");
        for (int i =0; i <codes.length;i++) {
            Article article = articles.get(Long.parseLong(codes[i]));
            if (article != null){
                 float prixHT = (float)article.prixHT/100;
                 int quantite = Integer.parseInt(quantites[i]);
                 float tauxTVA = (float)article.tauxTVA/100;
                 float cout = (prixHT + prixHT*tauxTVA)*quantite;
                 coutTotal += cout;
                    out.println("<tr>");
                    out.println("<td>"+article.codeBarre + "</td>");
                    out.println("<td>"+article.reference + "</td>");
                    out.println("<td>"+article.libelle + "</td>");
                    out.println("<td>"+ prixHT + "</td>");
                    out.println("<td>"+ quantite + "</td>");
                    out.println("<td>"+ tauxTVA + "</td>");
                    out.println("<td>"+cout + "</td>");
                    out.println("</tr>");
            }
        }
        out.println("</table>");
        out.println("<div>cout total : "+ coutTotal +" euros</div>" );
        out.println("<a class=\"badge badge-info\" href=''>Retour aux achats</a>");
        out.println("</body>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MapArticle singletonList = MapArticle.getInstance();
        Map<Long, Article> articles = singletonList.getListArticle();
        request.setAttribute("articles", articles);
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
    }
}
