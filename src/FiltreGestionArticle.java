import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/GestionArticle")
public class FiltreGestionArticle implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        if (req.getParameter("nom") == null & req.getParameter("pwd")== null){
            chain.doFilter(req, resp);
        }
        if(req.getParameter("nom").equals("admin") & req.getParameter("pwd").equals("admin")){
            chain.doFilter(req, resp);
        }
        throw new ServletException ("vous n'avez pas la permission d'acceder à cette page");
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
